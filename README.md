ELFDoor
=======

ELFdoor is a POC to demonstrate how to backdoor an ELF-File on Linux x86-64.
The concept is really simple and is nothing new. ELFdoor receives a file
to infect and the code (currently just relative addressed assembly code).
The program scans through program headers and when it finds one
which is of type PT_LOAD and flagged as PT_EXEC he calculates the pad which
occurs from the p_filesz of the current program header to the p_offset of
the next one. The final step is to inject the payload and modify the entry point
redirecting it to the injected code.
Of course if the injected code wants to preserve the original program behavior
he needs to accomplish that saving and replacing clobbered registers and maybe using
a fork() to spawn a new process, redirecting the father or child to the original
entry point etc...
